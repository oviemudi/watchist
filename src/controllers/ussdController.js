import sendEmergencyMessage from '../services/emergencyMessageService';
import Model from '../database/models';

const { User } = Model;

const root = ('*', (req, res) => {
  res.send('Welcome to Watcher USSD');
});

const handleUssd = ('*', async (req, res) => {
  let { /* phoneNumber, */ text } = req.body;
  text = text ? text.trim() : '';

  const defaultResponse = `What is your emergency?
  1. Robbery/Assault
  2. Accident
  3. Fire`;

  const locationResponse = `Enter location?
  1. Home
  2. Work
  3. Landmark`;

  const landMarkResponse = `Enter your location starting with "LOC". Example:
  LOC Obalende under bridge`;

  const enterId = 'CON Enter your ID';

  const messageSentResponse = 'Emergency message sent! Help is coming.';

  const STATUS = 200;

  // const ussdId = 'HS1489'; /* TODO: dynamically generate on register */

  let response;

  const getLastInput = (ussdText) => {
    const sections = ussdText.toUpperCase().split('*');
    const lastInput = sections[sections.length - 1];
    return lastInput;
  };

  const landmarkLocationInput = (locationText) => {
    const lastInput = getLastInput(locationText);
    return locationText.includes('*3') && lastInput.includes('LOC');
  };

  const testUssdIdInput = (ussdIdInput) => (/^[A-Za-z]{2}[0-9]{4,6}$/.test(ussdIdInput.trim()));

  const handleUssdAuth = async (textInput) => {
    const authErrorMessage = 'CON Incorrect user ID. Stay calm and try again';
    const inputs = textInput.split('*');
    inputs[1] = inputs[1] === '3' ? inputs[2] : inputs[1];
    const ussdId = getLastInput(text);
    if (!testUssdIdInput(ussdId)) {
      response = authErrorMessage;
      return;
    }
    try {
      const foundUser = await User.findOne({ ussdId });
      if (foundUser) {
        sendEmergencyMessage(ussdId, inputs[0], inputs[1]);

        response = `END ${messageSentResponse}`;
        return;
      }
      response = authErrorMessage;
      return;
    } catch (error) {
      console.error(error);
      response = 'CON Something went wrong. Please try again';
    }
  };

  if (!text) {
    response = `CON ${defaultResponse}`;
  } else if (text === '1' || text === '2' || text === '3') {
    response = `CON ${locationResponse}`;
  } else if (text.includes('1*1')) {
    /* set location of to home */
    response = enterId;
  } else if (text === '1*2') {
    response = enterId;
  } else if (text === '1*3' || text === '2*3' || text === '3*3') {
    response = `CON ${landMarkResponse}`;
  } else if (landmarkLocationInput(text)) {
    response = enterId;
  } else if (text === '2*1') {
    /* set location of to home */
    response = enterId;
  } else if (text === '2*2') {
    /* set location of to home */
    response = enterId;
  } else if (landmarkLocationInput(text)) {
    response = enterId;
    response = `END ${messageSentResponse}`;
  } else if (text === '3*1') {
    /* set location of to home */
    response = enterId;
  } else if (text === '3*2') {
    /* set location of to home */
    response = enterId;
  } else if (landmarkLocationInput(text)) {
    response = enterId;
    response = `END ${messageSentResponse}`;
  } else if (text.split('*').length === 3 && text.split('*')[1] !== '3') {
    await handleUssdAuth(text);
  } else if (text.split('*').length > 3) {
    await handleUssdAuth(text);
  } else {
    response = `CON INVALID INPUT.
    ${defaultResponse}`;
  }
  // console.log('response from ===>', locationInput);

  return res.status(STATUS).send(response);
});

export { root, handleUssd };
