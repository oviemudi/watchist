import Model from '../database/models';

const generateUssdId = (phoneNumber, firstName, lastName) => `${firstName[0]}${lastName[0]}${phoneNumber.substring(phoneNumber.length - 4)}`;

const createUser = ({ firstName, lastName, phoneNumber }) => Model.User.create({
  firstName, lastName, phoneNumber, ussdId: generateUssdId(phoneNumber, firstName, lastName),
});


export default createUser;
